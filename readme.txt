# xAccordion #

## HTML ##


```
#!html

<div class="accordion">
   <ul>
      <li>
         <a href="javascript:void(0);">Item 1</a>
         <div class="contentSlider">
            <p>Conteudo Item 1</p>
         </div>
      </li>
   </ul>
</div>
```
Essa é a estrutura básica do Accordion.

## Chamada Javascript ##


```
#!javascript

// Simples
   jQuery(".accordion").xAccordion();
		
// Com parametros
   jQuery(".accordion").xAccordion({
      openClose:true, 
      speed:200,
      activeItemName: "selecionado"
   });
```

## Parametros de chamada ##

openClose --> [true] para quando abrir um item, fechar todos os outros. [false] para abrir de maneira independente.
slide --> Efeito de slide [true], abrirá com efeito deslizar, [false] abrirá com mostrar e esconder rápido.
speed --> velocidade em ms, [1000] para 1 segundo, essa propriedade só é utilizada caso slide tenha sido marcada como [true].
openItens --> [true] para comecar o plugin com apenas 1 item aberto. [false] todos os itens virão fechados.
activeItemName --> Classe para item aberto.

**Alexandre Mattos**